# Equipe do Protocolo Tetrinet

## Protocolo TetriNET Game RFC 20191- [<span style="color:red">**ACESSE O PROTOCOLO COMPLETO AQUI**</span>](https://gitlab.com/tetrinetjs/protocol/-/jobs/225266901/artifacts/file/tetrinetProtocol.pdf)

## Protocolo WebSocket TetriNET RFC 20192- [<span style="color:red">**ACESSE O PROTOCOLO COMPLETO AQUI**</span>](https://gitlab.com/tetrinetjs/protocol/-/jobs/227946592/artifacts/file/websocket_tetrinet_rfc.pdf)

### Observações
* O código fonte do protocolo foi desenvolvido em LaTex. Arquivo < tetrinetProtocol.tex >. O código de compilação automática no ambiente do GitLab encontra-se no arquivo < .gitlab-ci.yml >  
* Para baixar o protocolo em pdf ou carregá-lo online click em CI / CD Jobs, selecione a última instância com o status de "passed". No canto direito ao centro click em download ou browser.

## Objetivos:
* Analisar o protocolo do jogo tetrinet
* Documentar o protocolo do jogo tetrinet

## Grupo 9 - membros
* Julio Alfredo Moreira Sousa Junior (Líder)
* Fernando Ferreira da Costa (Desenvolvedor)

## Tarefas da sprint 1 (25/03 a 31/03): 
* Documentar o ambiente de engenharia reversa do protocolo tetrinet:
    * Instalação e configuração de ferramentas
    * Servidor (tetrinet-server)
    * Cliente (tetrinet-client e/ou gtetrinet)
    * Wireshark
* Levantar documentação existente
    * Documentação do protocolo
    * Trechos de código
* Escrever snippets (trechos de código-fonte) para testes do protocolo:
    * Login de usuário


## Descrição dos Documentos
1. PROTOCOL.md: Documentação do protocolo TetriNET escrita em markdown (para facilitar a escrita) que, posteriormente, deverá ser traduzida para rfc utilizando-se a ferramenta https://ipv.sx/draftr/