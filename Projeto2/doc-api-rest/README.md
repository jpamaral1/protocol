# Documentação da API REST do Protocolo

A documentação foi feita com swagerr 2.0 para testar a api pelo swagger faça a clonagem do repositório e execute os seguintes comando dentro do diretório doc-api-rest

* ``swagger project start``
* ``swagger project edit``


Se for necessário instalar o swagger siga os passos do tutorial swagger - [tutorial swagger](https://scotch.io/tutorials/speed-up-your-restful-api-development-in-node-js-with-swagger "Clique e acesse agora!")


