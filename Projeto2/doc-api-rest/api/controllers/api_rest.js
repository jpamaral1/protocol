'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*
 Modules make it possible to import JavaScript files into your application.  Modules are imported
 using 'require' statements that give you a reference to the module.

  It is a good idea to list the modules that your application depends on in the package.json in the project root
 */
var util = require('util');

/*
 Once you 'require' a module you can reference the things that it exports.  These are defined in module.exports.

 For a controller in a127 (which this is) you should export the functions referenced in your Swagger document by name.

 Either:
  - The HTTP Verb of the corresponding operation (get, put, post, delete, etc)
  - Or the operationId associated with the operation in your Swagger document

  In the starter/skeleton project the 'get' operation on the '/hello' path has an operationId named 'hello'.  Here,
  we specify that in the exports of this module that 'hello' maps to the function named 'hello'
 */
module.exports = {
  rank: rank,
  playerpoint: playerpoint
};

/*
  Functions in a127 controllers used for operations should take two parameters:

  Param 1: a handle to the request object
  Param 2: a handle to the response object
 */

var players = JSON.parse(`{"rank": [{"Nick": "Fernando", "Pontos": 1230},{"Nick":"Joao", "Pontos": 1000},
{"Nick": "Jose", "Pontos": 900}, {"Nick": "Maria", "Pontos": 800}, {"Nick": "Ana", "Pontos": 500}]}`)


function rank(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
  // var name = req.swagger.params.name.value || 'stranger';
  // var rank = util.format('Hello, %s!', name);

  // this sends back a JSON response which is a single string
  res.json(JSON.stringify(players));
}


function playerpoint(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
  var nick = req.swagger.params.nick.value || 'stranger';

  if(nick === "Fernando"){
    res.json(JSON.stringify(players.rank[0]))
  }

  else if(nick === "Joao"){
    res.json(JSON.stringify(players.rank[1]))
  }

  else if(nick === "Jose"){
    res.json(JSON.stringify(players.rank[2]))
  }
  else if(nick === "Maria"){
    res.json(JSON.stringify(players.rank[3]))
  }
  else if(nick === "Ana"){
    res.json(JSON.stringify(players.rank[4]))
  }

  else res.json("Jogador inexistente")



}


